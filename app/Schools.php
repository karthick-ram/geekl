<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schools extends Model
{
    use SoftDeletes;


    protected $fillable = ['schools'];


    protected $dates = ['deleted_at'];

}
