<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\RequestSchool;

use App\Schools;

class RequestSchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('requestschool');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->validate($request, [
          
            'mobile' => 'required',
            'school' => 'required'

        ]);

        $school = new RequestSchool;

        $school->mobile = $request->mobile;
        $school->school = $request->school;
        $school->status = 0;

        $school->save();
        Session::flash('success', 'Scool request submitted.. we will let you know when done');
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $schools = RequestSchool::where('status', '=', 1)->get();


        return view('admin.show.request')->with('schools', $schools);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $school = RequestSchool::find($id);

        $school->status  = 1;

$schdata = $school->school;


        $sch = new Schools;

        $sch->schools = $schdata;

        $sch->save();
        Session::flash('success', 'School updated in registeration page!');

        return redirect()->back();




    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
