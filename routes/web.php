<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/admin/school/add', [

    'uses' => 'SchoolsController@index',
    'as' => 'create.school'

]);


Route::get('/request/school', [
'uses' => 'RequestSchoolController@index',
'as' => 'req.school'
]);

Route::post('/request/school', [
    'uses' => 'RequestSchoolController@create',
    'as' => 'store.school'
    ]);

    Route::get('/admin/request/school', [
        'uses' => 'RequestSchoolController@show',
        'as' => 'show.school'
        ])->middleware('admin');


    Route::post('/admin/request/school/{id}', [
        'uses' => 'RequestSchoolController@edit',
        'as' => 'edit.school'
        ])->middleware('admin');